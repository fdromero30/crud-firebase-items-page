import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { MainFrameModule } from './components/main-frame/main-frame.module';
import { LoginModule } from './components/login/login.component.module';
import { NotFoundComponent } from './shared/not-found/not-found.component';

export const cargarModuloPrincipal = () => MainFrameModule;
export const cargarModuloAutenticacion = () => LoginModule;

const routes: Routes = [
  {
    path: '', redirectTo: 'main', pathMatch: 'full'
    , canActivate: [AuthGuard]
  },
  {
    path: 'main', loadChildren: cargarModuloPrincipal
    , canActivate: [AuthGuard]
  },
  {
    path: 'login/ingreso', loadChildren: cargarModuloAutenticacion
  },
  {
    path: 'login/registro', loadChildren: cargarModuloAutenticacion
  },
  {
    path: 'not-found', component: NotFoundComponent, canActivate: [AuthGuard]
  },
  {
    path: '**', redirectTo: 'not-found'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
