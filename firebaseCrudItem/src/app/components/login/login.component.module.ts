
import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AngularFireAuth } from '@angular/fire/auth';
import { CommonModule } from '@angular/common';


@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [
        LoginRoutingModule,
        SharedModule,
        CommonModule
    ],
    providers: [AngularFireAuth],

})
export class LoginModule { }
