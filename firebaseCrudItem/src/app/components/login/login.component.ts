import { Component, OnInit } from '@angular/core';
import { UsuarioModel } from 'src/app/models/usuarioModel';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import { UtilService } from 'src/app/services/util.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    public user: UsuarioModel;
    public tipoPantalla: string;

    constructor(private router: Router
        , private auth: AuthService
        , private toast: ToastrService
        , private spinner: NgxSpinnerService
        , private route: ActivatedRoute
        , private utilService: UtilService
    ) {
        this.user = new UsuarioModel();
        this.tipoPantalla = this.router.url;
    }

    ngOnInit() {


    }

    /**
     * login con email y password
     * @author fromero 
     */
    onLogin(): void {
        this.spinner.show();
        this.auth.loginEmailUsuario(this.user.email, this.user.password)
            .then((res) => {
                setTimeout(() => {
                    this.spinner.hide();
                    this.mainRedirect()
                }, 300);
            }).catch(err => {

                this.catchException(err);
            });
    }

    /**
     * @author fromero
     * autenticación con google
     */
    onLoginGoogle(): void {

        this.auth.loginGmailUsuario().then(
            (res) => {
                this.mainRedirect()
            }).catch(
                err =>
                    this.catchException(err)
            );
    }
    /**
     * redireccòn a pagina principal
     * @author fromero
     */
    mainRedirect() {
        this.router.navigate(['/main/consultar']);

    }

    /**
     * metodo creación de usuario
     * @author fromero 
     */
    crearUsuario() {
        this.auth.registroUsuario(this.user.email, this.user.password)
            .then((res) => {
                this.mainRedirect();
            }).catch(err => this.catchException(err));
    }

    /**
     * 
     * @param err 
     */
    catchException(err) {
        this.toast.error(err.message, 'Error');
        if (err.code == 'auth/user-not-found') {
            this.router.navigate(['/login/registro']);
        }
        this.spinner.hide()
    }

    validarCorreo(): boolean {
        return this.utilService.validarCorreo(this.user.email);
    }
}