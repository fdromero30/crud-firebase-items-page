
import { NgModule } from '@angular/core';
import { MainFrameComponent } from './main-frame.component';
import { MainFrameRoutingModule } from './main-frame-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AngularFireAuth } from '@angular/fire/auth';




@NgModule({
    declarations: [
        MainFrameComponent,

    ],
    imports: [
        MainFrameRoutingModule,
        SharedModule

    ],

    providers: [AngularFireAuth],
})
export class MainFrameModule { }
