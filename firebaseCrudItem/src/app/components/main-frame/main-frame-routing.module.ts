import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewListModule } from '../view-list/view-list.module';
import { MainFrameComponent } from './main-frame.component';
import { CreateItemModule } from '../create-item/create-item.module';
import { ViewListApiModule } from '../view-list-api/view-list-api.module';


const ROUTES_MAIN: Routes = [
    {

        path: '', component: MainFrameComponent,
        children: [
            {
                path: '', loadChildren: () => ViewListModule,
            },
            {
                path: 'consultar', loadChildren: () => ViewListModule,
            }, {
                path: 'crear', loadChildren: () => CreateItemModule
            }, {
                path: 'consultar-api', loadChildren: () => ViewListApiModule
            }

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES_MAIN)],
    exports: [RouterModule]
})
export class MainFrameRoutingModule { }
