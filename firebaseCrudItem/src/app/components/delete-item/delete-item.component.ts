import { Component, OnInit } from '@angular/core';
import { ItemModel } from 'src/app/models/itemModel';
import { ConfirmationModalOptions } from 'src/app/shared/modal/modal.model';
import { ConstantesService } from 'src/app/services/constantes.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataApiService } from 'src/app/services/data-api.service';

@Component({
  selector: 'app-delete-item',
  templateUrl: './delete-item.component.html',
  styleUrls: ['./delete-item.component.css']
})
export class DeleteItemComponent implements OnInit {
  item: ItemModel;
  modalConfirmacionOptions: ConfirmationModalOptions;
  constructor(private activeModal: NgbActiveModal, private apiService: DataApiService

  ) {
    this.item = new ItemModel();
    this.modalConfirmacionOptions =
      new ConfirmationModalOptions(
        ConstantesService.TITULO_MODAL_CONFIRMACION
        , ConstantesService.MENSAJE_CONFIRMACION_ELIMINAR
        , ConstantesService.BOTON_SI
        , ConstantesService.BOTON_NO,
        null, false, false, 'Eliminar', null, 'btn-primary pull-right');
  }

  ngOnInit() {
  }

  onDelete(event) {
    if (event) {
      this.apiService.eliminarItem(this.item.idFb);
      this.activeModal.close();
    }
  }

  close() {
    this.activeModal.close();
  }
}
