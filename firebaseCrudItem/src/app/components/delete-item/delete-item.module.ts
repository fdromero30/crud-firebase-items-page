
import { NgModule } from '@angular/core';
import { DeleteItemComponent } from './delete-item.component';
import { DeleteItemRoutingModule } from './delete-item-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    DeleteItemComponent
  ],
  imports: [

    DeleteItemRoutingModule,
    SharedModule,
    CommonModule
  ],
  providers: [],
})
export class DeleteItemModule { }
