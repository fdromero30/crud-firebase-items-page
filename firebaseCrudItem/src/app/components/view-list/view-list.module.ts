
import { NgModule } from '@angular/core';
import { ViewListComponent } from './view-list.component';
import { ViewListRoutingModule } from './view-list-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModule } from '@angular/common';
import { DeleteItemComponent } from '../delete-item/delete-item.component';
import { EditItemComponent } from '../edit-item/edit-item.component';
import { DeleteItemModule } from '../delete-item/delete-item.module';
import { EditItemModule } from '../edit-item/edit-item.module';


@NgModule({
    declarations: [
        ViewListComponent
    ],
    imports: [
        ViewListRoutingModule,
        SharedModule,
        CommonModule,
        DeleteItemModule,
        EditItemModule
    ],
    providers: [],
    entryComponents: [DeleteItemComponent, EditItemComponent]
})
export class ViewListModule { }
