import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { ItemModel } from 'src/app/models/itemModel';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditItemComponent } from '../edit-item/edit-item.component';
import { DeleteItemComponent } from '../delete-item/delete-item.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/services/util.service';


@Component({
  selector: 'app-view-list',
  templateUrl: './view-list.component.html',
  styleUrls: ['./view-list.component.css']
})
export class ViewListComponent implements OnInit {

  data: any;
  items: ItemModel[];
  itemsFiltro: ItemModel[];
  filtro: string;
  totalRegistros: number;

  constructor(private apiService: DataApiService
    , private modalService: NgbModal
    , private spinner: NgxSpinnerService,
    private utilService: UtilService) {
    this.data = [];
    this.items = [];
  }

  ngOnInit() {
    this.listarDatoFire();
  }


  /**
   * Consulta inicial de datos en firebase
   * @author fromero
   * 
   */
  listarDatoFire() {

    this.spinner.show();
    this.apiService.traerTodosItems().subscribe(items => {
      this.items = items
      this.itemsFiltro = items;
      this.totalRegistros = this.items.length;
      this.spinner.hide();
    }
    );
  }

  /**
   * recibe la respuesta del componente de tarjetas y la acción del boton
   * @param event 
   * @author fromero
   */
  accion(event) {
    if (event) {
      if (event.accion == 'editar') {
        this.verModalEditar(event.obj);
      }
      else if (event.accion = 'eliminar') {
        this.verModalEliminar(event.obj);
      }
    }
  }

  /**
   * muestra el modal de edición
   * @param obj 
   * @author fromero
   */
  verModalEditar(obj) {
    const modalRef = this.modalService.open(EditItemComponent, { centered: false, size: 'lg' });
    modalRef.componentInstance.item = obj; // Objeto que se envìa al modal para ediciòn
  }

  /**
   * muestra el modal de eliminación
   * @param obj 
   * @author fromero
   */
  verModalEliminar(obj) {
    const modalRef = this.modalService.open(DeleteItemComponent, { centered: false, size: 'lg' });
    modalRef.componentInstance.item = obj; // Objeto que se envìa al modal para ediciòn

  }

  filtrarItems() {

    this.items = this.utilService.transform(this.itemsFiltro, this.filtro);
    this.totalRegistros = this.items.length;
  }
}
