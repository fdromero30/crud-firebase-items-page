
import { NgModule } from '@angular/core';
import { EditItemComponent } from './edit-item.component';
import { EditItemRoutingModule } from './edit-item-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModule } from '@angular/common';




@NgModule({
    declarations: [
        EditItemComponent
    ],
    imports: [
        EditItemRoutingModule,
        SharedModule,
        CommonModule
    ],
    providers: [],
})
export class EditItemModule { }
