import { Component, OnInit, Input } from '@angular/core';
import { ItemModel } from 'src/app/models/itemModel';
import { ConfirmationModalOptions } from 'src/app/shared/modal/modal.model';
import { ConstantesService } from 'src/app/services/constantes.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataApiService } from 'src/app/services/data-api.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})
export class EditItemComponent implements OnInit {
  @Input() item: ItemModel
  public itemEditar: ItemModel;
  modalConfirmacionOptions: ConfirmationModalOptions;
  constructor(private activeModal: NgbActiveModal, private apiService: DataApiService
    , private toast: ToastrService, private spinner: NgxSpinnerService, private utilService: UtilService) {

    this.item = new ItemModel();
    this.itemEditar = new ItemModel();
    this.modalConfirmacionOptions =
      new ConfirmationModalOptions(
        ConstantesService.TITULO_MODAL_CONFIRMACION
        , ConstantesService.MENSAJE_CONFIRMACION_EDITAR
        , ConstantesService.BOTON_SI
        , ConstantesService.BOTON_NO,
        null, true, false, 'Editar', null, 'btn-primary pull-right');
  }

  ngOnInit() {
    if (this.item) {
      this.itemEditar.id = this.item.id;
      this.itemEditar.title = this.item.title;
      this.itemEditar.idFb = this.item.idFb;
    }

  }

  close() {
    this.activeModal.close();
  }

  /**
   * petición al servicio de editar el item
   * @param item
   */
  onEditarItem(item) {
    if (item) {
      this.spinner.show();
      this.apiService.actualizarItm(this.itemEditar)
        .then((res) => {
          this.toast.success(ConstantesService.MENSAJE_EDICION_EXITO, ConstantesService.EXITO);
          this.item = new ItemModel();
          this.itemEditar = new ItemModel();
          this.close();
          this.spinner.hide();
        }).catch(err => {
          this.utilService.catchException(err);
        });


    }

  }
}
