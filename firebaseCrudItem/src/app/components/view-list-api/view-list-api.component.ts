import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { ItemModel } from 'src/app/models/itemModel';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-view-list-api',
  templateUrl: './view-list-api.component.html',
  styleUrls: ['./view-list-api.component.css']
})
export class ViewListApiComponent implements OnInit, OnDestroy {

  public data: any;
  public items: ItemModel[];
  public itemsFiltro: ItemModel[];
  private subcription: Subscription;
  public filtro: string;
  public totalRegistros: number;

  constructor(private apiService: DataApiService
    , private spinner: NgxSpinnerService, private utilService: UtilService) {
    this.data = [];
    this.items = [];
  }

  ngOnInit() {
    this.listarDatos();
  }

  ngOnDestroy() {
    this.subcription.unsubscribe();
  }

  /**
   * consulta los datos del apiEndPoint
   * @author fromero
   */
  listarDatos() {

    this.spinner.show();
    this.subcription = this.apiService.getData().subscribe((result: any) => {
      this.items = result;
      this.itemsFiltro = result;
      this.totalRegistros = this.items.length;
      this.spinner.hide();
    }, err => {
      console.log(err);
    })
  }


  /**
   * invoca la utilidad de filtro de items por titulo o id
   * @author fromero
   */
  filtrarItems() {

    this.items = this.utilService.transform(this.itemsFiltro, this.filtro);
    this.totalRegistros = this.items.length;

  }

}
