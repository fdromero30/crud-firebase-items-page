import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewListApiComponent } from './view-list-api.component';

const routes: Routes = [
  { path: '', component: ViewListApiComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewListApiRoutingModule { }
