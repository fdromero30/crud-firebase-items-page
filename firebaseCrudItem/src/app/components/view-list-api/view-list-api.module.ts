
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModule } from '@angular/common';
import { ViewListApiComponent } from './view-list-api.component';
import { ViewListApiRoutingModule } from './view-list-api-routing.module';


@NgModule({
    declarations: [
        ViewListApiComponent
    ],
    imports: [
        ViewListApiRoutingModule,
        SharedModule,
        CommonModule,
    ],
    providers: [],
    entryComponents: []
})
export class ViewListApiModule { }
