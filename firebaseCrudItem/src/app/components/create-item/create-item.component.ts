import { Component, OnInit } from '@angular/core';
import { ItemModel } from 'src/app/models/itemModel';
import { DataApiService } from 'src/app/services/data-api.service';
import { ConfirmationModalOptions } from 'src/app/shared/modal/modal.model';
import { ConstantesService } from 'src/app/services/constantes.service';
import { NgxSpinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/services/util.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.component.html',
  styleUrls: ['./create-item.component.css']
})
export class CreateItemComponent implements OnInit {

  public item: ItemModel;
  public modalConfirmacionOptions: ConfirmationModalOptions;

  constructor(
    private apiService: DataApiService
    , private utilService: UtilService
    , private toast: ToastrService
    , private spinner: NgxSpinnerService
    , private router: Router,
    private activeModal: NgbActiveModal) {
    this.item = new ItemModel();
    this.item.title = '';
    this.item.id = '';
    this.modalConfirmacionOptions =
      new ConfirmationModalOptions(
        ConstantesService.TITULO_MODAL_CONFIRMACION
        , ConstantesService.MENSAJE_CONFIRMACION_CREAR
        , ConstantesService.BOTON_SI
        , ConstantesService.BOTON_NO,
        null, true, false, 'Guardar', null, 'btn-primary pull-right');
  }

  ngOnInit() {
  }

  /**
   * Llamado a servicio para guardar nuevo registro
   * @author fromero
   * @param event 
   */
  onSaveItem(event: any) {
    this.item = event;
    this.spinner.show();
    this.apiService.crearItem(this.item).then((res) => {
      this.toast.success(ConstantesService.MENSAJE_CREACION_EXITO, ConstantesService.EXITO);

      this.item = new ItemModel();
      this.router.navigate(['/main/consultar']);
      this.spinner.hide();
    }).catch(err => {
      this.utilService.catchException(err);
    });
  }

  close() {
    this.activeModal.close();
  }
}
