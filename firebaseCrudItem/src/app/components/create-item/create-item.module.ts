
import { NgModule } from '@angular/core';
import { CreateItemComponent } from './create-item.component';
import { CreateItemRoutingModule } from './create-item-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [
    CreateItemComponent
  ],
  imports: [
    CreateItemRoutingModule,
    SharedModule,
    CommonModule
  ],
  providers: [],
})
export class CreateItemModule { }
