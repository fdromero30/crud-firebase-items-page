import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router,
    private afsAuth: AngularFireAuth
  ) {

  }

  /**
   * guarda de ingreso a rutas
   * @author fromero
   * @param route 
   * @param state 
   */
  canActivate() {


    if (sessionStorage.getItem('logged') && sessionStorage.getItem('logged') == 'true') {

      if (this.afsAuth.authState) {
        return true;
      } else {
        this.router.navigate(['/login/ingreso']);
      }
    } else {
      this.router.navigate(['/login/ingreso']);
    }
    return false;
  }
}
