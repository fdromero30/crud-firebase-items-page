import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { ItemModel } from '../models/itemModel';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { ConstantesService } from './constantes.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  public static API = "https://jsonplaceholder.typicode.com/todos";
  private listItems: AngularFirestoreCollection<ItemModel>;
  private items: Observable<ItemModel[]>;
  private itemDoc: AngularFirestoreDocument<ItemModel>;


  constructor(protected http: HttpClient
    , private fbDb: AngularFirestore,
    private toast: ToastrService, private spinner: NgxSpinnerService) {
    this.listItems = fbDb.collection<any>('items');
  }

  /***
   * consumo de API de prueba
   * @author fromero
   */
  public getData() {
    return this.http.get(DataApiService.API);
  }

  /**
   * Obtiene todos los items de firebase
   * @author fromero 
   */
  public traerTodosItems() {
    return this.items = this.listItems.snapshotChanges().pipe(map(changes => {
      return changes.map(action => {
        const data: ItemModel = action.payload.doc.data();
        data.idFb = action.payload.doc.id;
        return data;
      });
    }));

  }

  /**
   * agrega un itema a la lista de items
   * @param item 
   * @author fromero 
   */
  public crearItem(item: ItemModel) {

    this.spinner.show();
    const param = JSON.parse(JSON.stringify(item));
    return new Promise((resolve, reject) => {
      this.listItems.add(param)
        .then(
          res => resolve(res)
          ,
          err => reject(err)
        );
    });
  }

  /**
   * Actualiza un item en firebaseStore
   * @param item 
   * @author fromero
   */
  public actualizarItm(item: ItemModel) {
    let idItem = item.idFb;
    this.itemDoc = this.fbDb.doc<ItemModel>(`items/${idItem}`);
    const param = JSON.parse(JSON.stringify(item));
    return new Promise((resolve, reject) => {
      this.itemDoc.update(param).then(
        res => resolve(res)
        ,
        err => reject(err)
      );
    });
  }

  /**
   * Elimina un Item en FirebasStore
   * @param itemId 
   * @author fromero
   */
  public eliminarItem(itemId: string) {
    this.itemDoc = this.fbDb.doc<ItemModel>(`items/${itemId}`);
    return new Promise((resolve, reject) => {
      this.itemDoc.delete()
        .then(
          res => resolve(res)
          ,
          err => reject(err)
        );
    });

  }
}
