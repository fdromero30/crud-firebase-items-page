import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { auth } from 'firebase';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    isLogged: boolean;

    constructor(private afsAuth: AngularFireAuth) { }

    /**
     * registro con email de usuario
     */
    registroUsuario(email: string, pass: string) {
        return new Promise((resolve, reject) => {
            this.afsAuth.auth.createUserWithEmailAndPassword(email, pass)
                .then(userData => resolve(userData),
                    err => reject(err));
        })
    }

    /**
     * login con email
     * @author fromero
     */
    loginEmailUsuario(email: string, password: string) {
        return new Promise((resolve, reject) => {
            this.afsAuth.auth.signInWithEmailAndPassword(email, password)
                .then(
                    userData => resolve(userData),
                    err => reject(err)
                );
        });
    }
    /**
     * login en aplicacion con GOOGLE
     * @author fromero
     */
    public loginGmailUsuario() {
        return this.afsAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    }

    /**
     * log Out aplicacion
     * @author fromero
     */
    logOutUser() {
        this.afsAuth.auth.signOut();
    }

    /**
     * valida si el usuario  està logueado
     * @author fromero
     */
    isAuth() {
        return this.afsAuth.authState.pipe(map(auth => auth));
    }
}
