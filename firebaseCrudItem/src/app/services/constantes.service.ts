
export class ConstantesService {

    public static MENSAJE_CONFIRMACION_CREAR = '¿Está seguro de guardar el registro?';
    public static MENSAJE_CONFIRMACION_ELIMINAR = '¿Está seguro de eliminar el registro?';
    public static MENSAJE_CONFIRMACION_EDITAR = '¿Está seguro de editar el registro?';
    public static BOTON_SI = 'SI';
    public static BOTON_NO = 'NO';
    public static TITULO_MODAL_CONFIRMACION = 'Confirmación';
    public static MENSAJE_CREACION_EXITO = 'Se guardó el registro exitosamente';
    public static MENSAJE_EDICION_EXITO = 'Se editó el registro exitosamente';
    public static EXITO = 'Éxito';

}