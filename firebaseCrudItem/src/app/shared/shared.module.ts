
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { RouterModule } from '@angular/router';

import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
import { TarjetaRegistroComponent } from './tarjeta-registro/tarjeta-registro.component';
import { FormItemComponent } from './form-item/form-item.component';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationModalComponent } from './modal/confirmationModal.component';
import { SearchPipe } from '../pipes/search.pipe';
import { OnlyNumberDirective } from '../directives/onlyNumber';
import { UppercaseInputDirective } from '../directives/uppercase';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
    declarations: [
        NavBarComponent
        , TarjetaRegistroComponent
        , FormItemComponent
        , ConfirmationModalComponent
        , SearchPipe
        , OnlyNumberDirective
        , UppercaseInputDirective
        , NotFoundComponent

    ],
    imports: [
        RouterModule,
        FormsModule,
        CommonModule,
        ToastrModule.forRoot(),
        NgxSpinnerModule,
        NgxPaginationModule,
        NgbModule,
    ],
    providers: [AngularFireAuth, NgbActiveModal],
    exports: [NavBarComponent
        , FormsModule
        , NgxSpinnerModule
        , TarjetaRegistroComponent
        , NgxPaginationModule
        , FormItemComponent
        , ConfirmationModalComponent
        , SearchPipe
        , OnlyNumberDirective
        , UppercaseInputDirective
        , NotFoundComponent],

})
export class SharedModule { }
