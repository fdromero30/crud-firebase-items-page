import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { ItemModel } from 'src/app/models/itemModel';

@Component({
  selector: 'app-tarjeta-registro',
  templateUrl: './tarjeta-registro.component.html',
  styleUrls: ['./tarjeta-registro.component.scss']
})
export class TarjetaRegistroComponent implements OnInit, OnChanges {

  @Input() objeto: ItemModel[] = [];
  @Input() pantalla: string;
  @Input() filtroBusqueda: string;
  @Output() result = new EventEmitter();


  constructor(
    private router: Router,
  ) {

  }

  ngOnInit() {
  }

  ngOnChanges(): void {
  }


  accion(event, objeto) {
    this.result.emit({ obj: objeto, accion: event });
  }

}
