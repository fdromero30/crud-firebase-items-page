import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarjetaRegistroComponent } from './tarjeta-registro.component';

describe('TarjetaRegistroComponent', () => {
  let component: TarjetaRegistroComponent;
  let fixture: ComponentFixture<TarjetaRegistroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TarjetaRegistroComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarjetaRegistroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnchanges', () => {
    component.ngOnChanges();
  });

  it('cambiarPaginaTestA', () => {
    component.cambiarPagina('a');
    expect(component.inicio).toEqual(0);
  });

  it('cambiarPaginaTestS', () => {

    component.objeto = ['prueba', 'P'];
    component.cambiarPagina('s');
    expect(component.inicio).toEqual(0);
  });

  it('cambiarPaginaTestS', () => {

    component.objeto = ['prueba', 'P', 'c', '', '', ''];
    component.cambiarPagina('s');
    expect(component.inicio).toEqual(0);
  });

  it('cambiarPaginaTestDefault', () => {

    component.objeto = ['prueba', 'P', 'c', '', '', ''];
    component.cambiarPagina('6');
    expect(component.fin).toEqual(36);
  });

  it('paginador', () => {

    component.objeto = ['prueba', 'P', 'c', '', '', '', ''];
    component.paginador();
  });

  it('irDetalle', () => {

    component.irADetalle('', '');
  });


});
