import { ConfirmationModalOptions } from './modal.model';
import { Component, Input, OnInit, EventEmitter, Output, ViewChild, OnChanges } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'confirmation-app-modal',
  templateUrl: './confirmationModal.component.html',
  styleUrls: ['./confirmationModal.component.scss']
})
export class ConfirmationModalComponent implements OnInit, OnChanges {

  @Input() modalOptions: ConfirmationModalOptions;
  @Input() data: any;
  @Output() modalResponse = new EventEmitter();


  constructor(
    public activeModal: NgbActiveModal, private modalService: NgbModal
  ) {

  }

  ngOnChanges() {

  }
  ngOnInit() {

    this.modalOptions = this.modalOptions;

  }
  open(content) {
    this.activeModal = this.modalService.open(content, { centered: false });
  }
  aceptar() {
    this.activeModal.close();
    this.modalResponse.emit([true, this.data]);
  }

  cancelar() {
    this.activeModal.close();
    this.modalResponse.emit([false, this.data]);
  }
}
