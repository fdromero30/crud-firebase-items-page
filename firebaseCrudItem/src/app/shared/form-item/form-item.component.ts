import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationModalOptions } from '../modal/modal.model';
import { ItemModel } from 'src/app/models/itemModel';

@Component({
  selector: 'app-form-item',
  templateUrl: './form-item.component.html',
  styleUrls: ['./form-item.component.css']
})
export class FormItemComponent implements OnInit, OnChanges {

  @Input() objeto: ItemModel;
  @Input() accionBoton: any;
  @Output() respuesta = new EventEmitter;
  @Input() modalOptions: ConfirmationModalOptions
  @Input() pagina: string;

  constructor(
    private router: Router,
  ) {

    this.modalOptions =
      new ConfirmationModalOptions('Confirmación', '',
        'SI', 'NO', null, true, false, '', null, 'pull-right');


  }

  ngOnInit() {
  }

  ngOnChanges(): void {
  }

  accion(event) {
    if (event[0]) {
      this.respuesta.emit(this.objeto);
    }
  }

  validarFormulario() {
    if (this.objeto.id && this.objeto.id != '' && this.objeto.title && this.objeto.title != '') {
      this.modalOptions.disabled = false;
    }
  }



}
