import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit, OnDestroy {

  public isLogged: boolean;
  private subscription: Subscription[];

  @Output() logout = new EventEmitter();

  constructor(private router: Router, private auth: AuthService) {
    this.subscription = [];
    this.getCurrentUser();
  }

  ngOnInit() {

  }
  ngOnDestroy() {

    this.subscription.forEach(subs => {
      subs.unsubscribe();
    })
  }

  /**
   * llamado a servicio de cerrar sesion
   * @author fromero
   */
  logOut() {
    this.auth.logOutUser();
    this.router.navigate(['login/ingreso']);
  }

  /**
   * redireccion to login
   * @author fromero
   */
  login() {
    this.router.navigate(['login/ingreso']);
  }

  /**
   * obtener usuarioa ctual
   * @author fromero
   */
  getCurrentUser() {
    const subs = this.auth.isAuth().subscribe(a => {
      if (a) {
        this.isLogged = true;
        this.auth.isLogged = true;
        sessionStorage.setItem('logged', 'true');
      } else {
        this.isLogged = false;
        this.auth.isLogged = false;
        sessionStorage.setItem('logged', 'false');
      }
    });
    this.subscription.push(subs);
  }


}
