# CRUD-Firebase-Items-Page

Proyecto de prueba para michaelPage CRUD ITEMS FIREBASE,

Proyecto Angular, con conexión a firebase Database, y usando bootstrap 4.

# app

 # components

 Estructura de componentes donde se encuentran las vistas de la aplicación y contenedor general.

 # directives

 Carpeta que contiene las directivas usadas en la aplicaciòn para transformación de datos

 # guards

 Contiene la guarda de la aplicación para validar la autenticación por rutas

 # models

 Contiene los modelos utilizados por los componentes de la aplicación

 # pipes

 Contiene un pipe de busqueda utilziado para filtrar items

 # services

 Contiene el servicio de autenticación y crud de datos de firebase

 # shared
 Contiene componentes y modulos reutilizados varias veces en el aplicativo




